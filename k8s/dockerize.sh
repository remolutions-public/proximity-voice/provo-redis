#!/bin/sh

# correct for actual prod build tag (e. g. v0.0.0)
ENV=$CI_ENVIRONMENT_NAME
if [ -z "$ENV" ]; then
  ENV=STAGING
fi
LOWERCASE_ENV=$(echo "$ENV" | tr '[:upper:]' '[:lower:]')

IMAGE_TAG=$CI_PIPELINE_ID-$ENV
if [ "$ENV" = "PROD" ]; then
  IMAGE_TAG=$CI_COMMIT_TAG
fi

BASE_PATH=$(dirname \"$0\" | tr -d '"')/..

echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" | tr -d '\n' > /kaniko/.docker/config.json


IMAGE_NAME=redis-commander
IMAGE_PATH=$CI_REGISTRY/$CI_PROJECT_PATH/$IMAGE_NAME:$IMAGE_TAG


/kaniko/executor  --context $BASE_PATH/container/$IMAGE_NAME \
                  --dockerfile $BASE_PATH/container/$IMAGE_NAME/Dockerfile \
                  --destination "$IMAGE_PATH"


echo "pushed image $IMAGE_PATH"

