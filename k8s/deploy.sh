#!/bin/sh

ENV=$CI_ENVIRONMENT_NAME
if [ -z "$ENV" ]; then
  ENV=STAGING
fi
LOWERCASE_ENV=$(echo "$ENV" | tr '[:upper:]' '[:lower:]')
BASE_PATH=$(dirname \"$0\" | tr -d '"')
NAMESPACE="provo-$LOWERCASE_ENV"


mkdir -p $HOME/.kube
if [ "$ENV" = "PROD" ]; then
  KUBE_CONFIG=$KUBE_CONFIG_PROD
  NAMESPACE=$NAMESPACE_PROD
  REDIS_PASSWORD=$REDIS_PASSWORD_PROD
elif [ "$ENV" = "STAGING" ]; then
  KUBE_CONFIG=$KUBE_CONFIG_STAGING
  NAMESPACE=$NAMESPACE_STAGING
  REDIS_PASSWORD=$REDIS_PASSWORD_STAGING
fi
cp $KUBE_CONFIG $HOME/.kube/config
chmod 700 $HOME/.kube/config



IMAGE_TAG=$CI_PIPELINE_ID-$ENV
if [ "$ENV" = "PROD" ]; then
  IMAGE_TAG=$CI_COMMIT_TAG
fi
IMAGE_NAME=redis-commander
IMAGE_PATH=$CI_REGISTRY/$CI_PROJECT_PATH/$IMAGE_NAME:$IMAGE_TAG



SECRET_NAME=provo-redis-secrets
if [ -z "$(kubectl get secrets $SECRET_NAME -n $NAMESPACE --no-headers --ignore-not-found)" ]; then
  kubectl create secret generic $SECRET_NAME -n $NAMESPACE --from-literal=REDIS_PASSWORD=$REDIS_PASSWORD
fi



VALUES_BASE_YAML=$BASE_PATH/values.yaml
VALUES_ENV_YAML=$BASE_PATH/values.$LOWERCASE_ENV.yaml


helm template $CI_PROJECT_NAME -n $NAMESPACE \
              -f $VALUES_BASE_YAML \
              -f $VALUES_ENV_YAML \
              --set admin.image=$IMAGE_PATH \
              $BASE_PATH > $BASE_PATH/deploy-template.yaml


kubectl apply -f $BASE_PATH/deploy-template.yaml -n $NAMESPACE


echo "app deployed!"
