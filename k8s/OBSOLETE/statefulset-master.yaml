apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ .Values.name }}-master
  namespace: {{ .Values.namespace }}
  labels:
    app.kubernetes.io/name: redis
    helm.sh/chart: redis-17.11.3
    app.kubernetes.io/instance: {{ .Values.name }}
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: master
spec:
  replicas: {{ .Values.masterReplicas }}
  selector:
    matchLabels:
      app.kubernetes.io/name: redis
      app.kubernetes.io/instance: {{ .Values.name }}
      app.kubernetes.io/component: master
  serviceName: {{ .Values.name }}-headless
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app.kubernetes.io/name: redis
        helm.sh/chart: redis-17.11.3
        app.kubernetes.io/instance: {{ .Values.name }}
        app.kubernetes.io/managed-by: Helm
        app.kubernetes.io/component: master
      annotations:
        checksum/configmap: 9dde98e978f6da62dfd9ac575777c9089116977b86f186f7f14c17f1a375426b
        checksum/health: 5268162215d96a374d84784fc6c55f21bbcc769a40c74b688239581441446d3e
        checksum/scripts: e24fdb4cfdd8029ab3397762762471aa82e32780161e3b50b7b0f75f72b4a483
        checksum/secret: 372015d749e94a0a8bac8674940b7a17a09a65bb3b1d987c8240b8538ef6f253
    spec:
      
      securityContext:
        fsGroup: 1001
      serviceAccountName: {{ .Values.name }}
      affinity:
        podAffinity:
          
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchLabels:
                    app.kubernetes.io/name: redis
                    app.kubernetes.io/instance: {{ .Values.name }}
                    app.kubernetes.io/component: master
                topologyKey: kubernetes.io/hostname
              weight: 1
        nodeAffinity:
          
      terminationGracePeriodSeconds: 30
      containers:
        - name: redis
          image: docker.io/bitnami/redis:7.0.11-debian-11-r12
          imagePullPolicy: "IfNotPresent"
          securityContext:
            runAsUser: 1001
          command:
            - /bin/bash
          args:
            - -c
            - /opt/bitnami/scripts/start-scripts/start-master.sh
          env:
            - name: BITNAMI_DEBUG
              value: "false"
            - name: REDIS_REPLICATION_MODE
              value: master
            - name: ALLOW_EMPTY_PASSWORD
              value: "no"
            - name: REDIS_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.secretObjectName }}
                  key: REDIS_PASSWORD
            - name: REDIS_TLS_ENABLED
              value: "no"
            - name: REDIS_PORT
              value: "6379"
          ports:
            - name: redis
              containerPort: {{ .Values.port }}
          livenessProbe:
            initialDelaySeconds: 20
            periodSeconds: 5
            # One second longer than command timeout should prevent generation of zombie processes.
            timeoutSeconds: 6
            successThreshold: 1
            failureThreshold: 5
            exec:
              command:
                - sh
                - -c
                - /health/ping_liveness_local.sh 5
          readinessProbe:
            initialDelaySeconds: 20
            periodSeconds: 5
            timeoutSeconds: 2
            successThreshold: 1
            failureThreshold: 5
            exec:
              command:
                - sh
                - -c
                - /health/ping_readiness_local.sh 1
          resources:
            limits: {}
            requests: {}
          volumeMounts:
            - name: start-scripts
              mountPath: /opt/bitnami/scripts/start-scripts
            - name: health
              mountPath: /health
            - name: redis-data
              mountPath: /data
            - name: config
              mountPath: /opt/bitnami/redis/mounted-etc
            - name: redis-tmp-conf
              mountPath: /opt/bitnami/redis/etc/
            - name: tmp
              mountPath: /tmp
      volumes:
        - name: start-scripts
          configMap:
            name: {{ .Values.name }}-scripts
            defaultMode: 0755
        - name: health
          configMap:
            name: {{ .Values.name }}-health
            defaultMode: 0755
        - name: config
          configMap:
            name: {{ .Values.name }}-configuration
        - name: redis-tmp-conf
          emptyDir: {}
        - name: tmp
          emptyDir: {}
  volumeClaimTemplates:
    - metadata:
        name: redis-data
        labels:
          app.kubernetes.io/name: redis
          app.kubernetes.io/instance: {{ .Values.name }}
          app.kubernetes.io/component: master
      spec:
        accessModes:
          - "ReadWriteOnce"
        resources:
          requests:
            storage: "8Gi"
