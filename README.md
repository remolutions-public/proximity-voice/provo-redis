# What is this?
This redis db is used to store player information centrally for backend apis to consume


# Official Provo website
https://provo.remolutions.com/

[![alt text](https://provo.remolutions.com/assets/logos/Provo_64p.png "Provo")](https://provo.remolutions.com/)



# How to manage the redis db
- install the "Another Redis Desktop Manager": https://www.electronjs.org/apps/anotherredisdesktopmanager



## Provo Discord Server
- https://discord.gg/NJTFTgNvzG



## Donation/Support
If you like my work and want to support further development or just to spend me a coffee please

[![alt text](https://i.imgur.com/Y0XkUcd.png "Paypal $")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=S3WQNNSVY8VAL)

[![alt text](https://i.imgur.com/xezX26q.png "Paypal €")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VQRPA46YADD9J)



# How to render a new updated template
- run in gitbash
```shell
# https://github.com/bitnami/charts/blob/main/bitnami/redis/values.yaml
sh upgrade/upgrade.sh

```
