#!/bin/sh

BASE_PATH=$(dirname \"$0\" | tr -d '"')


helm template provo-redis \
  --set auth.password=secretpassword \
  -f $BASE_PATH/values.yaml \
  oci://registry-1.docker.io/bitnamicharts/redis > $BASE_PATH/uprade-template.yaml
