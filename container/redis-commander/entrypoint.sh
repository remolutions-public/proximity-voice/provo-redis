#!/bin/sh


if [ -z "$GUI_PORT" ]; then
  GUI_PORT=8080
fi
FLAGS=" --port $GUI_PORT"

if [ ! -z "$REDIS_PORT" ]; then
  FLAGS="$FLAGS --redis-port $REDIS_PORT"
fi

if [ ! -z "$REDIS_HOST" ]; then
  FLAGS="$FLAGS --redis-host $REDIS_HOST"
fi

if [ ! -z "$REDIS_PASSWORD" ]; then
  FLAGS="$FLAGS --redis-password $REDIS_PASSWORD"
fi

if [ ! -z "$SENTINEL_HOST" ]; then
  FLAGS="$FLAGS --sentinel-host $SENTINEL_HOST"
fi

if [ ! -z "$SENTINEL_PORT" ]; then
  FLAGS="$FLAGS --sentinel-port $SENTINEL_PORT"
fi

if [ ! -z "$SENTINEL_GROUP_NAME" ]; then
  FLAGS="$FLAGS --sentinel-name $SENTINEL_GROUP_NAME"
fi

if [ ! -z "$SENTINEL_PASSWORD" ]; then
  FLAGS="$FLAGS --sentinel-password $SENTINEL_PASSWORD"
fi

redis-commander $FLAGS
